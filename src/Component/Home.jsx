import React from "react";
import { Card, Button } from "react-bootstrap";
import { Link } from "react-router-dom";

export default function Home({ cards }) {

  return (
    <div>
      {cards.map((cards) => (
        <Card style={{ width: "18rem", display: "inline-block", margin: "3%" }}>
          <Card.Img variant="top" src={cards.image} />
          <Card.Body>
            <Card.Title>{cards.Name}</Card.Title>
            <Card.Text>
            Travelling can often take a long time, especially when great distances need to be covered.
             People often enjoy travelling abroad for holidays. 
            </Card.Text>
            <Link to = {`/Path/${cards.id}`}><Button variant="primary">See more</Button></Link>
          </Card.Body>
        </Card>
      ))}
    </div>
  );
}
