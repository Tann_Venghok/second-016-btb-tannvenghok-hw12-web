import React from "react";
import {
  Link,
  useLocation
} from "react-router-dom";

function useQuery() {
    return new URLSearchParams(useLocation().search);
  }
  
  export default function Account() {
    let query = useQuery();
  
    return (
      <div style ={{textAlign : "left", margin : "3%"}}>
        <div>
          <h2>Accounts</h2>
          <ul>
            <li>
              <Link to="/account?name=netflix">Netflix</Link>
            </li>
            <li>
              <Link to="/account?name=spotify">Spotify</Link>
            </li>
            <li>
              <Link to="/account?name=Hulu">Hulu</Link>
            </li>
            <li>
              <Link to="/account?name=apple-music">Apple Music</Link>
            </li>
          </ul>
  
          <Child name={query.get("name")} />
        </div>
      </div>
    );
  }
  
  function Child({ name }) {
    return (
      <div>
        {name ? (
          <h3>
            Your <code>selected account</code> in the query string is <span style ={{color : 'red'}}>{name}</span>
          </h3>
        ) : (
          <h3>There is no name in the query string</h3>
        )}
      </div>
    );
  }
