import React from "react";
import {
  Switch,
  Route,
  Link,
  useParams,
  useRouteMatch,
} from "react-router-dom";

export default function Video() {
  let { path, url } = useRouteMatch();
  return (
    <div style ={{textAlign : "left", margin : "2%"}}>
      <ul>
        <li>
          <Link to={`${url}/animation`}>Animation</Link>
        </li>
        <li>
          <Link to={`${url}/movie`}>Movie</Link>
        </li>
      </ul>
      <h3>Please choose an option</h3>

      <Switch>
        <Route path={`${path}/animation`}>
          <Animation />
        </Route>
        <Route path={`${path}/movie`}>
          <Movie />
        </Route>
      </Switch>
    </div>
    // </Router>
  );
}

function Topic() {
  let { topicId } = useParams();

  return (
    <div style ={{textAlign : "left", margin : "2%", color : 'red'}}>
      <h3>{topicId}</h3>
    </div>
  );
}

function Animation() {
  let { path, url } = useRouteMatch();

  return (
    <div>
      <ul>
        <li>
          <Link to={`${url}/LoveStory`}>Love Story</Link>
        </li>
        <li>
          <Link to={`${url}/Drama`}>Drama</Link>
        </li>
        <li>
          <Link to={`${url}/Crime`}>Crime</Link>
        </li>
      </ul>
      <h3>Please choose a category</h3>

      <Switch>
        <Route path={`${path}/:topicId`}>
          <Topic />
        </Route>
      </Switch>
    </div>
  );
}

function Movie() {
  let { path, url } = useRouteMatch();

  return (
    <div >
      <ul>
        <li>
          <Link to={`${url}/Action`}>Action</Link>
        </li>
        <li>
          <Link to={`${url}/Adventure`}>Adventure</Link>
        </li>
        <li>
          <Link to={`${url}/Horror`}>Horror</Link>
        </li>
      </ul>
      <h3>Please choose a category</h3>

      <Switch>
        <Route path={`${path}/:topicId`}>
          <Topic />
        </Route>
      </Switch>
    </div>
  );
}
