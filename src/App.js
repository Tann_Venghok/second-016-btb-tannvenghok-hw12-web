import React, { Component } from 'react'
import "./App.css";
import "bootstrap/dist/css/bootstrap.min.css";
import Menu from "./Component/Menu";
import { Route, BrowserRouter as Router, Switch } from "react-router-dom";
import Home from "./Component/Home";
import Video from "./Component/Video";
import Account from "./Component/Account";
import Auth from "./Component/Auth";
import MainPage from "./Component/MainPage";
import Path from './Component/Path';

export default class App extends Component {
  constructor(){
    super();
    this.state = {
        cards : [
            {
                id : 1,
                Name : "ABC",
                image : "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQ7DHLvshN3PXFOExEL0-oydzy_aLqbL3KhLwkKeq9sTTbtwFZP&usqp=CAU"
            },
            {
                id : 2,
                Name : "DEF",
                image : "https://img.freepik.com/free-vector/vector-illustration-mountain-landscape_1441-56.jpg?size=626&ext=jpg"
            },
            {
                id : 3,
                Name : "GHI",
                image : "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQwCVKTaThF7uRPLKed1gtmSwQIlYno3K830JnWPisUKiyRw5U8&usqp=CAU"
            }
        ]
    }
  }
  render() {
    return (
      <div className="App">
        <Router>
          <Menu />
          <Switch>
            <Route path="/" exact component={MainPage} />
            <Route path="/Home" render = {() => <Home cards={this.state.cards}/> } />
            <Route path="/Video" component={Video} />
            <Route path="/Account" component={Account} />
            <Route path="/Auth" component={Auth} />
            <Route path="/Path/:id" render = {(props) => <Path {...props} cards={this.state.cards}/> } />
          </Switch>
        </Router>
      </div>
    );
  }
}

